#!/usr/bin/env ruby
def spaces(length)
  ' ' * (length / 2 - 4)
end

def bar(style, length)
  bar = style * (length - 2)
  puts '*' + bar + '*'
end

def ornament_head(char, length = 40)
  puts spaces(length) + '__-o8o-__'
  bar(char, length)
end

def ornament_tail(char, length = 40)
  bar(char, length)
  puts spaces(length) + '~~=====~~'
  puts
end

def ornament(char, length = 40)
  ornament_head(char, length)
  yield
  ornament_tail(char, length)
end

def player_interaction
  ornament_head("-")
  puts 'To set your hanging, enter letter:'
  puts '"r" for rock'
  puts '"p" for paper'
  puts '"s" for scissors'
  ornament_tail("-")
  puts 'enter q for exit'
  input = STDIN.gets.chomp
  input !='q' ? input : false
end

handings = ['r', 'p', 's']
names = { 'r' => 'rock', 'p' => 'paper', 's' => 'scissors' }
beats = { 'r' => 's', 'p' => 'r', 's' => 'p' }

player_score = 0
computer_score = 0

while player = player_interaction
  if player != "q"
    rand_num = rand 3
    computer = handings[rand_num]
    puts "Computer's move is #{names[computer]}"
    ornament("-", 15) do
      if player == computer
        # it is a tie
        puts "It's a tie, try again!"
        player_score +=1
        computer_score += 1
      else
        if beats[player] == computer
          puts 'You won!'
          player_score += 1
        else
          puts 'Computer won!'
          computer_score += 1
        end
      end
    end
  else
    puts 'player left the game'
  end
end

ornament_head("=")
puts '== Final Result =='
puts "You vs. computer #{player_score}:#{computer_score}"
if player_score == computer_score
  puts "It's a tie."
else
  if player_score > computer_score
    puts 'You won!'
  else
    puts 'Computer won!'
  end
end
ornament_tail('=')